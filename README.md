# pysis.sub.bash

This script creates a bash script for remote execution of `pysis`.

The script is an updated version of the script included in the 
`rwth-tools` repository by Martin C Schwarzer, which is available
[here](https://github.com/polyluxus/rwth-tools).
